/// <reference types="cypress" />

describe('Basic Tests', () =>{

	beforeEach(() => {
		cy.viewport(1280,720)
		cy.visit('https://codedamn.com')
	})

	it('Every basic element exists', ()=> {
		cy.visit('https://codedamn.com')
	})
	it('Login page looks good',() => {
		cy.contains('Sign in').click()
		cy.contains('Sign in to codedamn').should('exist')
		cy.contains('Email address / Username').should('exist')
		cy.contains('Password').should('exist')
		cy.contains('Forgot your password?').should('exist')
	})

	it('The login page links work', () => {
		// 1. Sign in page
		cy.contains('Sign in').click()

		// 2. Password reset page
		cy.contains('Forgot your password?').click({force: true})

		// 3. Verify your page URL
		cy.url().should('include','/password-reset')

		// cy.url().then((value) => {
		// 	cy.log('The current real URL is:', value)
		// })

		// console.log(111) : masuk ke inspact

		// 4. Go back, on the sign in the page
		cy.go('back')

		// cy.contains('Create one').should('exist')
		// cy.url().should('include','/register')
	})

	it('Login should display correct error', () => {
		cy.contains('Sign in').click()

		cy.get('[data-testid=username]').type('admin')
		cy.get('[data-testid=password]').type('admin')

		cy.get('[data-testid="login"]').click()

		// cy.contains('Unable to authorize').should('exist')
	})

	it('Login should work fine', () => {
		cy.contains('Sign in').click()

		cy.get('[data-testid=username]').type('iosdev')
		cy.get('[data-testid=password]').type('iosdev')

		cy.get('[data-testid="login"]').click()


	})

})

//Apa saja yg dipelajari
// cy.visit
// cy.contains : cek data
// cy.get : memanggil data
// should : banyak elemen yang bisa dipakai
// cy.viewport : mengubah ukuran tampilan web, bisa pakai 2
// click() : perintah klik atau membuka halaman yg dituju
// it.only() : hanya perintah it tersebut yang ke run
// cy.url() : untuk pindah halaman
// cy.url().should('include','/password-reset') : untuk / saja
// cy.url().should('eq','https://codedamn.com/password-reset')
// cy.log('Going to forgot password')
